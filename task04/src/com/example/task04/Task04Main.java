package com.example.task04;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Task04Main {

    public static void main(String[] args) throws IOException {

        new BufferedReader(new InputStreamReader(System.in))
                .lines()
                .flatMap(x -> Stream.of((x.split("[\\P{L}&&\\D]"))))
                .filter(x->!x.isEmpty())
                .map(String::toLowerCase)
                .collect(Collectors.toMap(k -> k, v -> 1, Integer::sum))
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(10)
                .forEach(x -> System.out.print(x.getKey() + "\n"));
    }

}
