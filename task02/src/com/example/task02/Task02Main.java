package com.example.task02;

import java.util.stream.IntStream;

public class Task02Main {

    public static void main(String[] args) {


        cycleGrayCode(3)
                .limit(10)
                .forEach(System.out::println);


    }

    public static int grayCode(int g){
        return g ^ (g >> 1);
    }
    public static IntStream cycleGrayCode(int n) {
        if(n<1 || n>16)
            throw new IllegalArgumentException();

        return IntStream.iterate(0,k->k+1==Math.pow(2,n)?0:k+1).map(Task02Main::grayCode); // your implementation here

    }

}
