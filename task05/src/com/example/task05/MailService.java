package com.example.task05;

import java.util.*;
import java.util.function.Consumer;

public class MailService<T> implements Consumer<Message>{

    private final List<Message> list = new ArrayList<>();

    public MailService() {
    }

    ;


    @Override
    public void accept(Message message) {
        list.add(message);
    }



    public Map<String, List<T>> getMailBox() {
        Map<String, List<T>> mailBox = new HashMapWithGetNonNull<>();
        list.stream().forEach(a -> {
            List<T> listT = mailBox.get(a.getTo());
            if (listT.equals(Collections.emptyList()))
                listT = new LinkedList<>();
            listT.add((T) a.getContent());
            mailBox.put(a.getTo(), listT);
        });

        return mailBox;
    }

    private static class HashMapWithGetNonNull<K,V> extends HashMap<K,V>{
        @Override
        public V get(Object key){
            V v = super.get(key);
            try {
                if (v == null) v = (V)Collections.emptyList();
            } catch (ClassCastException e) {}
            return v;
        }
    }

}
