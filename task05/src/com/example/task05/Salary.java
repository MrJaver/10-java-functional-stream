package com.example.task05;

public class Salary implements Message{
    private String From;
    private String To;
    private Integer Salary;
    public Salary(){
        super();
    };

    public Salary(String From,String To,Integer salary){
        super();
        this.From=From;
        this.To=To;
        this.Salary=salary;
    }

    @Override
    public String getFrom() {
        return this.From;
    }

    @Override
    public String getTo() {
        return this.To;
    }

    @Override
    public Integer getContent() {
        return this.Salary;
    }
}
