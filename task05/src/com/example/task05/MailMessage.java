package com.example.task05;

public class MailMessage implements Message {
    private String From;
    private String To;
    private String Content;
    public MailMessage(String From,String To,String Content){
        this.From=From;
        this.Content=Content;
        this.To=To;
    }

    public MailMessage() {

    }

    @Override
    public String getFrom() {
        return this.From;
    }

    @Override
    public String getTo() {
        return this.To;
    }

    @Override
    public String getContent() {
        return this.Content;
    }
}
