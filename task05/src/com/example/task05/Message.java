package com.example.task05;

public interface Message<T> {
    public String getFrom();

    public String getTo();

    public T getContent();

}
